// Low-level signal handling
#include <csignal>
#include <cstdlib>
#include <iostream>
#include <spdlog/spdlog.h>
#include <thread>
#include <mutex>
#include <set>

#include <czmq.h>

// WebSocket++
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

// Boost UUID
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

// RapidJSON
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

// Shorten websocketpp prefix
namespace wspp = websocketpp;

// Shorten rapidjson prefix
namespace json = rapidjson;

using namespace fmt::literals;

class WSCockpitServer {
public:
  using WSServer_t = wspp::server<wspp::config::asio>;
  using Connections_t =
    std::set<wspp::connection_hdl, std::owner_less<wspp::connection_hdl>>;

  WSCockpitServer(unsigned short port = 0) : mcPort(port) {
    spdlog::info("Starting WebSocket server on port {}", mcPort);

    setLogLevels();
    mServer.init_asio();

    mServer.set_message_handler([](auto && /*handle*/, auto && /*msg*/) {
      spdlog::error("Message rx not yet implemented");
    });

    mServer.set_open_handler([&](auto &&handle) {
      std::lock_guard<std::mutex> lg{mConnections_mutex};
      mConnections.insert(handle);
    });

    mServer.set_close_handler([&](auto &&handle) {
      std::lock_guard<std::mutex> lg{mConnections_mutex};
      mConnections.erase(handle);
    });

    mServer.set_reuse_addr(true);
    mServer.listen(static_cast<unsigned short>(mcPort));
    mServer.start_accept();

    mThread = std::thread{&WSServer_t::run, &mServer};
  }

  void broadcast(std::string const &message) {
    // Possible deadlock?
    std::lock_guard<std::mutex> lg{mConnections_mutex};

    for (auto const &handle : mConnections) {
      mServer.send(handle, message, wspp::frame::opcode::TEXT);
    }
  }

  ~WSCockpitServer() {
    spdlog::info("Stopping WebSocket server");
    mServer.stop();
    mThread.join();
  }

  static auto buildPing() -> std::string {
    // ...
    return R"({"command":"ping"})";
  }

private:
  void setLogLevels() {
    mServer.set_access_channels(wspp::log::alevel::all);

    mServer.clear_access_channels(wspp::log::alevel::frame_payload);
    mServer.clear_access_channels(wspp::log::alevel::message_payload);
    mServer.clear_access_channels(wspp::log::alevel::frame_header);
  }

  unsigned short const mcPort = 0;

  WSServer_t mServer;
  std::thread mThread;

  std::mutex mConnections_mutex;
  Connections_t mConnections;
};

class SalsaApp {
public:
  SalsaApp(std::string &channel, unsigned short const port)
      : mcSubscribe(">tcp://localhost:5001"), mcChannel(channel), mcPort(port),
        mWebSocket(port) {

    spdlog::info("Starting app with channel {} on port {}", mcChannel, mcPort);
    spdlog::info("Use the following message to open channel via bridge:");
    spdlog::info("");
    spdlog::info(buildBridgeOpenCommand());
    spdlog::info("---");

    mpSocket = zsock_new_sub(mcSubscribe.c_str(), mcChannel.c_str());
    if (mpSocket == nullptr) {
      throw std::runtime_error("Could not connect to SALSA");
    }

    mpPoller = zpoller_new(mpSocket, nullptr);
  }

  auto buildBridgeOpenCommand() -> std::string {

    // Build hello key
    json::Document doc{json::kObjectType};
    auto &docAlloc = doc.GetAllocator();

    json::Value channel{json::kStringType};
    channel.SetString(mcChannel.c_str(),
      static_cast<json::SizeType>(mcChannel.length()), docAlloc);

    doc.AddMember("command", "open", docAlloc);
    doc.AddMember("channel", channel, docAlloc);
    doc.AddMember("payload", "websocket-stream1", docAlloc);
    doc.AddMember("path", "/", docAlloc);
    doc.AddMember("port", mcPort, docAlloc);

    // Serialize JSON document
    json::StringBuffer buf;
    json::Writer<json::StringBuffer> writer{buf};
    doc.Accept(writer);

    return buf.GetString();
  }

  void run() {
    spdlog::info("Application is running");
    while (zctx_interrupted == 0) {
      void *pPolled = zpoller_wait(mpPoller, -1);

      if (zpoller_terminated(mpPoller)) {
        break;
      }

      if (pPolled == nullptr) {
        continue;
      }

      spdlog::info("rx'd from SALSA socket {}", pPolled);

      // Receive and parse message
      zmsg_t *pMessage = zmsg_recv(pPolled);

      char *pSubscribedTo = zmsg_popstr(pMessage);
      char *pID = zmsg_popstr(pMessage);
      char *pData = zmsg_popstr(pMessage);

      // std::string fmtMsg = "{};{};{}"_format(pSubscribedTo, pID, pData);
      // mWebSocket.broadcast(fmtMsg);
      mWebSocket.broadcast(std::string(pData));

      // Destroy all allocated memory
      zmsg_destroy(&pMessage);

      free(pSubscribedTo);
      free(pID);
      free(pData);
    }
  }

  ~SalsaApp() {
    spdlog::info("Stopping application");
    zpoller_destroy(&mpPoller);
    zsock_destroy(&mpSocket);
  }

private:
  std::string const mcSubscribe;
  std::string const mcChannel;
  unsigned short const mcPort;

  zpoller_t *mpPoller = nullptr;
  zsock_t *mpSocket = nullptr;

  WSCockpitServer mWebSocket;
};

auto main(int argc, char *const *argv, char *const * /*envp*/) -> int {

  spdlog::info("Usage: {} [channel [port]]", argv[0]);
  spdlog::info("Starting...");
  spdlog::info("Finishing and cleaning up...");

  std::signal(SIGINT, [](int /*sig*/) {});

  std::string channel = "salsa";
  unsigned short port = 9999;

  // Set channel, if provided
  if (argc >= 2) {
    channel = argv[1];
  }

  // Convert port, if provided
  if (argc >= 3) {
    char *portStr = argv[2];
    char *pEnd = nullptr;
    long lport = std::strtol(portStr, &pEnd, 10);

    if (pEnd == portStr || *pEnd != '\0') {
      spdlog::critical("Cannot convert port {}", portStr);
      return 1;
    }

    if (lport < 1 || lport > 65535) {
      spdlog::critical("Invalid port {}", portStr);
      return 1;
    }

    port = static_cast<unsigned short>(lport);
  }

  // Create and run salsa receiver application
  try {
    SalsaApp app{channel, port};
    app.run();

  } catch (std::runtime_error &ex) {
    spdlog::critical(ex.what());
    return 1;
  }

  spdlog::info("Bye!");
  return 0;
}
