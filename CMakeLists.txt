cmake_minimum_required(VERSION 3.10)
project(zmq2ws-cpp
  VERSION 0.20201128.0
)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_CXX_STANDARD 17)

# Libraries ===================================================================
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules")

find_package(PkgConfig REQUIRED)
pkg_check_modules(CZMQ REQUIRED libczmq)

find_package(spdlog REQUIRED)
find_package(websocketpp REQUIRED)
find_package(fmt REQUIRED)
find_package(RapidJSON REQUIRED)
find_package(Boost REQUIRED)

# Compilation =================================================================
add_compile_options("-march=native")

# Force disable fmtlib bundled with spdlog
add_compile_options("-DSPDLOG_FMT_EXTERNAL")

# Enable all warnings (where available)
if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  add_compile_options("-Weverything")
  add_compile_options("-Wno-pedantic" "-Wno-c++98-compat")
else()
  add_compile_options("-Wall" "-Wextra")
endif()

# Force enable colors for Ninja generator
if(CMAKE_GENERATOR STREQUAL "Ninja")
  add_compile_options("-fdiagnostics-color=always")
endif()

# Executables =================================================================
# Main executable
add_executable(zmq2ws src/zmq2ws.cc)
target_link_libraries(zmq2ws PRIVATE spdlog::spdlog fmt pthread boost_system czmq)
