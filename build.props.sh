# shellcheck disable=SC2034

#: Build executables ----------------------------------------------------------

#: Build system: make (ninja)
: "${BUILD_SYSTEM:=make}"

#: C compiler: gcc (clang)
: "${CC:=gcc}"

#: C++ compiler: g++ (clang++)
: "${CXX:=g++}"

#: Build properties -----------------------------------------------------------

#: Build processes: nproc (any int)
: "${BUILD_NPROC:=$(nproc)}"

#: CMake flags: <none> (any valid cmake flag)
: "${CMAKE_FLAGS:=}"

#: Clean build: false (true)
: "${BUILD_CLEAN:=false}"

#: Misc properties ------------------------------------------------------------

#: Change build directory
#BUILD_DIR="/tmp/ndm-zmq2ws-build"
