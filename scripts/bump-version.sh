#!/usr/bin/bash

set -euo pipefail

#: Check if CMakeLists.txt exists in $PWD
if ! [[ -e CMakeLists.txt ]]; then
  printf -- '\033[1;31m::\033[0;1m CMakeLists.txt not found in %s\033[0m\n' "$PWD"
  exit 1
fi
printf -- '\033[1;34m::\033[0;1m CMakeLists.txt found in %s\033[0m\n' "$PWD"

#: Check if CMakeLists.txt contains correct project string
if ! grep -q '^  VERSION [0-9.]\+' CMakeLists.txt; then
  printf -- '\033[1;31m::\033[0;1m Could not find correct version string\033[0m\n'
  exit 1
fi
printf -- ' version string found\n'

#: Bump version string to today
TODAY=$(date +%Y%m%d)
CURRENT_VERSION=$(grep -oP '(?<=  VERSION )[0-9]\.[0-9]{8}\.[0-9]+' CMakeLists.txt)

NEW_BUMP="0.$TODAY."

#: If minor has been bumped today, bump patch
if grep -q 'VERSION 0\.'"$TODAY"'\.' CMakeLists.txt; then
  printf -- ' minor already bumped today, bumping patch\n' #: r/theyknew
  CURRENT_BUMP=${CURRENT_VERSION##*.}
  NEW_BUMP+="$((CURRENT_BUMP + 1))"
else
  NEW_BUMP+='0'
fi

printf -- '\033[1;34m::\033[0;1m Bumping version\033[0m\n'
printf -- ' old \033[1m%s\033[0m\n new \033[1m%s\033[0m\n' \
  "$CURRENT_VERSION" "$NEW_BUMP"

sed -i -e 's/^\(  VERSION\) [0-9.]\{12,\}/\1 '"$NEW_BUMP"'/' CMakeLists.txt
