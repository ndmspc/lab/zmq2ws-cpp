#!/bin/bash
# vim: sw=2 ts=2 sts=2 et ft=sh fenc=utf-8 fdm=marker
set -euo pipefail

# Parse command line {{{
while [[ $# != 0 ]]; do
  case $1 in
    clean)
      BUILD_CLEAN=true
      printf -- ':: ARG - Running clean build\n'
      ;;
  esac
  shift
done
# }}}

# Get build information {{{
# TODO nested dirname vs. this vs. grep vs. rev|cut
PROJECT_ROOT=$(readlink -m "$0")
PROJECT_ROOT=${PROJECT_ROOT%/*/*} # strip .../scripts/make.sh

BUILD_DIR="${PROJECT_ROOT}/build"

if [[ -z ${PROJECT_ROOT} ]]; then
  printf >&2 -- ':: Project root not found!\n'
  exit 1
fi

# Source in default values
# shellcheck source=../build.props.sh
. "${PROJECT_ROOT}/build.props.sh"

if [[ $BUILD_SYSTEM == 'ninja' ]]; then
  CMAKE_FLAGS+='-GNinja'
fi
# }}}

# Check for dependencies {{{
CAN_CONTINUE=true
for DEPENDENCY in 'cmake' "$BUILD_SYSTEM" "$CC" "$CXX"; do
  printf -- ':: Checking command %s\n' "$DEPENDENCY"
  if ! command -v "${DEPENDENCY}" &>/dev/null; then
    printf >&2 -- ' - MISSING command %s\n' "$DEPENDENCY"
    CAN_CONTINUE=false
  fi
done

if [[ $CAN_CONTINUE != true ]]; then
  printf -- ':: Cannot continue, exiting\n'
  exit 1
fi
# }}}

# Print build information {{{
printf -- ':: Project root: %s\n' "${PROJECT_ROOT}"
printf -- ':: Build dir   : %s\n' "${BUILD_DIR}"
printf -- ':: Build system: %s\n' "${BUILD_SYSTEM}"
printf -- ':: CMake flags : %s\n' "${CMAKE_FLAGS}"
# }}}

# Create and enter build directory {{{
if [[ -e $BUILD_DIR ]] && [[ $BUILD_CLEAN == true ]]; then
  printf -- ':: Clean build, removing build directory\n'
  rm -rf "$BUILD_DIR"
fi

mkdir -p "${BUILD_DIR}"
pushd "${BUILD_DIR}"
# }}}

# BUILD STARTS HERE ===========================================================
# Run build {{{
printf -- ':: running cmake\n'
cmake --version
cmake ${CMAKE_FLAGS} "${PROJECT_ROOT}"

printf -- ':: building with %d job(s)\n' "$(nproc)"
${BUILD_SYSTEM} --version
${BUILD_SYSTEM} -j "$(nproc)"
# }}}

popd
printf -- ':: Done\n'
